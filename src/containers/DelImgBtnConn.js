
import {connect} from 'react-redux'
import DelImgBtn from '../components/DelImgBtn'
import {refreshImgs} from '../actions'

import axios from 'axios'
var querystring = require('querystring');

const mapStateToProps = (state)=>({
  modifyPermission: state.modifyPermission
})

const mapDispatchToProps = (dispatch, ownProps)=>({
  delAction: ()=>{
   async function aaa(){
     await axios.get('/imageDel?'+querystring.stringify({url:ownProps.imgUrl}));
     var res = await axios.get('/getMyImages');
     var imgs = res.data.imgs.map(e=>{return e.url});
     dispatch(refreshImgs(imgs));
   };
   aaa();
 }
})

export default connect(mapStateToProps, mapDispatchToProps)(DelImgBtn)
