
import {connect} from 'react-redux'
import Tabs from '../components/Tabs'
import {permissionSet} from '../actions'
import {permissionClear} from '../actions'

import axios from 'axios'

const mapStateToProps = (state)=>({
  userInfo: state.userInfo
})

const mapDispatchToProps = (dispatch)=>({
  modifyPermissionSet: ()=>{
    async function aaa(){
      var res = await axios.get('/getMyImages');
      var imgs = res.data.imgs.map(e=>{return e.url});
      dispatch(permissionSet(imgs));
    };
    aaa();
  },
  modifyPermissionClear: ()=>{
    async function bbb(){
      var res = await axios.get('/getAllImages');
      var imgs = res.data.imgs.map(e=>{return e.url});
      dispatch(permissionClear(imgs));
    };
    bbb();
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Tabs)
