
import {connect} from 'react-redux'
import Image from '../components/Image'

const mapStateToProps = (state, ownProps)=>({
  mofifyPermission: state.modifyPermission,
  imgUrl: ownProps.imgUrl
})

const mapDispatchToProps = ()=>({

})

export default connect(mapStateToProps, mapDispatchToProps)(Image)
