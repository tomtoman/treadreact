
import {connect} from 'react-redux'
import Images from '../components/Images'
import {refreshImgs} from '../actions'
import {initUserInfo} from '../actions'

import axios from 'axios'

const mapStateToProps = (state)=>({
  urls: state.imgs
})

const mapDispatchToProps = (dispatch)=>({
  refresh:()=>{

    axios.get('/getUserInfo').then((userInfo)=>{

      console.log(userInfo);

      async function aaa(){
        var res = await axios.get('/getAllImages');
        console.log(res);
        var imgs = res.data.imgs.map(e=>{return e.url});
        dispatch(refreshImgs(imgs));
        dispatch(initUserInfo(userInfo.data));
      };
      aaa();
    }).catch((zzz)=>{

      console.log(zzz);

      async function bbb(){
        var res = await axios.get('/getAllImages');
        var imgs = res.data.imgs.map(e=>{return e.url});
        dispatch(refreshImgs(imgs));
      };
      bbb();
    });
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Images)
