
import {connect} from 'react-redux'
import AddImgRow from '../components/AddImgRow'
import {refreshImgs} from '../actions'

import axios from 'axios'
var querystring = require('querystring');

const mapStateToProps = (state)=>({
  modifyPermission: state.modifyPermission
})

const mapDispatchToProps = (dispatch)=>({
  addAction: (e)=>{
    async function aaa(){
      await axios.get('/imageAdd?'+querystring.stringify({url:e.target.querySelector('input').value}));
      var res = await axios.get('/getMyImages');
      var imgs = res.data.imgs.map(e=>{return e.url});
      dispatch(refreshImgs(imgs));
    };
    aaa();
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(AddImgRow)
