
import React from 'react'

const AddImgRow = ({modifyPermission, addAction})=>(
  <div class={modifyPermission?'d-block':'d-none'}>

    <form onSubmit={
      event=>{
        event.preventDefault()
        addAction(event)
      }
    }>
      <input placeholder='image url' class='form-control m-3' />
      <input type='submit' value='add' class='btn btn-primary m-3' />
    </form>
  </div>
)

export default AddImgRow
