
import React from 'react'
import PropTypes from 'prop-types'

const DelImgBtn = ({modifyPermission, delAction})=>(
  <div class={modifyPermission?'d-block':'d-none'}>
    <div class='card-body text-center'>
      <button class='btn btn-primary' onClick={delAction}>
        Delete
      </button>
    </div>
  </div>
)

DelImgBtn.propTypes = {
  modifyPermission: PropTypes.bool.isRequired,
  delAction: PropTypes.func.isRequired
}

export default DelImgBtn
