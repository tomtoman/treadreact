
import React from 'react'
import DelImgBtnConn from '../containers/DelImgBtnConn'

const Image = ({mofifyPermission, imgUrl})=>(
  <div class='col-6 col-lg-4 col-xl-3 p-3'>
    <div class='card'>
      <img src={imgUrl} class='img-fluid card-img-top' alt='Not Applicable' />
      <DelImgBtnConn imgUrl={imgUrl} />
    </div>
  </div>
)

export default Image
