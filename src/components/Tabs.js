
import React from 'react'
import PropTypes from 'prop-types'

const Tabs = ({ modifyPermissionSet, modifyPermissionClear, userInfo})=>(
  <div>
    <div class={userInfo.userId==null?'d-block':'d-none'} >
      <div class='text-center'>
        <a href='http://54.202.53.80:8001/login' class='btn btn-danger' >Login</a>
      </div>
    </div>
    <div class={userInfo.userId!=null?'d-block':'d-none'} >
      <div class='text-center'>
        <p class='h4 d-inline'>Hello</p>
        <span class='badge badge-secondary' >{userInfo.userId==null?'not login':userInfo.username}</span>
      </div>
      <div class='text-center'>
        <button onClick={modifyPermissionSet} class='btn btn-dark m-3' >My Images</button>
      </div>
    </div>
    <div class='text-center'>
      <button onClick={modifyPermissionClear} class='btn btn-dark m-3' >All Images</button>
    </div>
  </div>
)

Tabs.propTypes = {
  modifyPermissionSet: PropTypes.func.isRequired,
  modifyPermissionClear: PropTypes.func.isRequired
}

export default Tabs
