
import React from 'react'

import ImageConn from '../containers/ImageConn'

class Images extends React.Component{

    componentDidMount(){
      this.props.refresh();
    }

    render(){
      var urls = this.props.urls;
      return (
        <div class='row'>
          {urls.map(url=>
            <ImageConn imgUrl={url} />
          )}
        </div>
      );
    }
}

export default Images
