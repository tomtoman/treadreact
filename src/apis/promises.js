
var axios = require('axios')
var querystring = require('querystring');

async function getAllImages(cb){
  var res = await axios.get('/getallimages');
  var imgs = res.data.imgs.map(e=>e.url);
  cb(imgs);
}

async function getMyImages(cb){
  var res = await axios.get('/getMyImages');
  var imgs = res.data.imgs.map(e=>e.url);
  cb(imgs);
}

async function imageAdd(url){
  var ttt = await axios.get('/imageAdd?'+querystring.stringify({url:url}))
}

async function imageDel(url){
  var ttt = await axios.get('/imageDel?'+querystring.stringify({url:url}))
}

async function getUserInfo(){
  var ttt = await axios.get('/getUserInfo')
  console.log(ttt)
}
