import React from 'react';
import './App.css';

import myRecuder from './reducers'
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import ImagesConn from './containers/ImagesConn'
import TabsConn from './containers/TabsConn'
import AddImgRowConn from './containers/AddImgRowConn'

const store = createStore(myRecuder);

function App() {
  return (
    <div class='container'>
      <div class='row bg bg-primary aaa'>
      </div>
      <div class='row'>
        <Provider store={store}>
          <div class='col-4 col-lg-2'>
            <TabsConn />
            <AddImgRowConn />
          </div>
          <div class='col-8 col-lg-10'>
            <ImagesConn />
          </div>
        </Provider>
      </div>
    </div>
  );
}

export default App;
