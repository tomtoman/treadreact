
var initState = {
  modifyPermission: false,
  userInfo: {},
  imgs: []
}

export default (state=initState, action)=>{
  switch (action.type) {
    case 'permissionSet':
      return {
        modifyPermission:true,
        userInfo: state.userInfo,
        imgs: action.imgs
      }
    case 'permissionClear':
      return{
        modifyPermission:false,
        userInfo: state.userInfo,
        imgs: action.imgs
      }
    case 'refreshImgs':
      return{
        modifyPermission:state.modifyPermission,
        userInfo: state.userInfo,
        imgs: action.imgs
      }
    case 'initUserInfo':
      return{
        modifyPermission:state.modifyPermission,
        userInfo: action.userInfo,
        imgs: state.imgs
      }
    default:
      return state
  }
}
