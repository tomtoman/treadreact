
export const permissionSet = (imgs)=>({
  type: 'permissionSet',
  imgs: imgs
})

export const permissionClear = (imgs)=>({
  type: 'permissionClear',
  imgs: imgs
})

export const refreshImgs = (imgs)=>({
  type: 'refreshImgs',
  imgs: imgs
})

export const initUserInfo = (userInfo)=>({
  type: 'initUserInfo',
  userInfo: userInfo
})
